#!/bin/bash

clear

# Constants
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

# Fungsi untuk menambahkan baris pada file log
add_log() {
    echo "$(date +'%m/%d/%y') $(date +'%T') $1: $2" >> log.txt
}

# Buat file untuk menampung data user
mkdir -p users
touch ./users/user.txt

# Tampilkan prompt untuk menerima input username dan password
is_password_validated=false
username=""

until [ $is_password_validated == true ]
do
    echo -e "${YELLOW}---- LOGIN ----"

    if [[ $username == "" ]]
    then
        echo -n "Username : " 
        read username
    else
        echo "Username : $username"
    fi

    echo -n "password : 🔑" 
    read -s password

    echo -e "\n${YELLOW}------------------"
    echo -e '\n'

    clear

    # Mengecek apakah username telah terdaftar
    a=$(awk -v user="$username" -v pass="$password" '$0 ~ user {if ($1 == user && $2 == pass) print "true"; else print "false";}' ./users/user.txt)
    if [[ $a == "true" ]]
    then
        clear
        echo -e "${GREEN}Login Berhasil\n${YELLOW}"
        add_log "LOGIN" "INFO User $username logged in"
        is_password_validated=true
        continue
    else
        clear
        echo -e "${RED}Login GAGAL\n"
        add_log "LOGIN" "ERROR Failed login attemp on user $username"
        username=""
        continue
    fi
done

while true
do
    read r1 r2
    
    # Memilih menu
    if [[ $r1 == "dl" ]]
    then
        clear 

        dirname=$(date +'%F'_$username)
        img_num=0
        [ -d $dirname ] && rm -r $dirname

        if [ -f $dirname.zip ]
        then
            unzip -qq -P $password $dirname.zip
            rm $dirname.zip
            latest_img=$(echo $(ls $dirname | tail -1) | tr -d -c 0-9)
            img_num=$(echo $latest_img | sed 's/^0*//')
        fi

        mkdir -p $dirname

        img_num=$((img_num + 1))
        for ((num=$img_num; num<=$r2; num=num+1))
        do
            filename=$(printf "%02d" $num)
            let prog=$((20 * num / $r2))
            progress=$(for ((i=1; i<=$prog; i=i+1)); do echo -n "#";done)$(for ((i=1; i<=20-$prog; i=i+1)); do echo -n " ";done)
            echo -ne "Downloading '$progress' ($num / $r2) \r"
            curl -L -s https://loremflickr.com/320/240 --output $dirname/PIC_$filename.jpg
        done
        
        echo -e "\033[K"
        echo -e "${GREEN}Download selesai\n${YELLOW}"

        files=($(ls $dirname))
        files=(${files[@]/#/$dirname/})

        zip -r -qq -P $password "$dirname.zip" "${files[@]:0:$r2}"

    elif [[ $r1 == "att" ]]
    then
        clear

        echo -e "${CYAN}USER $username telah login sebanyak $(awk -v user=LOGIN.+INFO.+$username '$0 ~ user' log.txt  | wc -l) kali"
        echo -e "Ada $(awk -v user=LOGIN.+ERROR.+$username '$0 ~ user {print user}' log.txt | wc -l) kali percobaan login yang gagal${YELLOW}"
    fi
done