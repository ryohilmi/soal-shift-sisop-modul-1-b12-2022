#!/bin/bash

clear

# Constants
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

# Fungsi untuk mengecek apakah password sudah memenuhi kriteria
check_password() {
    clear 
    
    is_eight_character=false
    has_capital=false
    is_alphanumeric=false
    is_not_same=false

    if [ $2 != $1 ]
    then
        is_not_same=true
    else
        echo -e "${RED}Password tidak boleh sama dengan username"
    fi

    if echo $2 | grep -P -q ".{8,}" 
    then
        is_eight_character=true
    else
        echo -e "${RED}Password harus terdiri dari setidaknya 8 karakter"
    fi

    if echo $2 | grep -P -q "(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]+"
    then
        has_capital=true
    else
        echo -e "${RED}Password harus memiliki setidaknya 1 huruf kecil dan 1 huruf besar"
    fi

    if echo $2 | grep -P -q "(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]+" 
    then
        is_alphanumeric=true
    else
        echo -e "${RED}Password harus terdiri dari angka dan huruf"
    fi

    if [[ $is_eight_character == true && $has_capital == true && $is_alphanumeric == true && $is_not_same == true ]]
    then
        is_password_validated=true
    fi

    echo -e '\n'
}

# Fungsi untuk menambahkan baris pada file log
add_log() {
    echo "$(date +'%m/%d/%y') $(date +'%T') $1: $2" >> log.txt
}

# Buat file untuk menampung data user
mkdir -p users
touch ./users/user.txt
touch log.txt

# Tampilkan prompt untuk menerima input username dan password
is_password_validated=false
username=""

until [ $is_password_validated == true ]
do
    echo -e "${YELLOW}---- REGISTER ----"

    if [[ $username == "" ]]
    then
        echo -n "Username : " 
        read username
    else
        echo "Username : $username"
    fi
           
    # Mengecek apakah username telah terdaftar
    a=$(awk -v user="$username" '$0 ~ user {if ($1 == user) print 0}' ./users/user.txt)
    if [[ $a ]]
    then
        clear
        echo -e "${RED}Username sudah digunakan\n"
        add_log "REGISTER" "ERROR User already exists"
        username=""
        continue
    fi

    echo -n "password : 🔑" 
    read -s password

    echo -e "\n${YELLOW}------------------"
    echo -e '\n'

    # Cek apakah password sudah memenuhi kriteria
    check_password "$username" "$password"
done

echo "$username $password" >> ./users/user.txt
add_log "REGISTER" "INFO User $username registered successfully"

echo "Berhasil mendaftarkan $username"