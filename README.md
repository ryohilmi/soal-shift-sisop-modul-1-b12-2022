# sisop-modul-1-b12-2022

## Kelompok B12:
- [Alya Shofarizqi Inayah](https://gitlab.com/https://gitlab.com/alyashofarizqi): 5025201113
- [Ryo Hilmi Ridho](https://gitlab.com/ryohilmi): 5025201192
- [Surya Abdillah](https://gitlab.com/Surya_Abdillah): 5025201229

# Soal 1
### Penjelasan Soal
Pada soal 1, praktikan diminta untuk membuat sebuah login system. Terdapat 2 buah script bash yang harus dibuat, yaitu `register.sh` dan `main.sh`, masing-masing digunakan untuk daftar dan login. Data pengguna yang telah terdaftar disimpan ke dalam sebuah file bernama `users.txt`. Setelah berhasil daftar, pengguna dapat memasukkan perintah `dl N` ataupun `att`. Perintah `dl N` akan mendownload beberapa gambar sebanyak N dan meng-compressnya ke dalam sebuah zip yang diberi password yang sama dengan password pengguna, sedangkan perintah `att` akan menampilkan banyaknya percobaan login pada akun pengguna saat itu, baik itu gagal ataupun berhasi.

### Penyelesaian
Seluruh fitur register diimplementasikan di dalam file `register.sh`. Script tersebut akan membuat sebuah folder `users` dan file `users.txt`
```sh
mkdir -p users
touch ./users/user.txt
touch log.txt
```
> Untuk menampung data user yang akan melakukan registrasi, dibutuhkan sebuah file txt bernama `users.txt`
  
```sh
is_password_validated=false
username=""

until [ $is_password_validated == true ]
do
    ...
done
```
Input password akan dilakukan berulang-ulang selama password yang dimasukkan belum memenuhi kriteria yang diminta

```sh
echo -e "${YELLOW}---- REGISTER ----"

if [[ $username == "" ]]
then
    echo -n "Username : " 
    read username
else
    echo "Username : $username"
fi

# Mengecek apakah username telah terdaftar
a=$(awk -v user="$username" '$0 ~ user {if ($1 == user) print 0}' ./users/user.txt)
if [[ $a ]]
then
    clear
    echo -e "${RED}Username sudah digunakan\n"
    add_log "REGISTER" "ERROR User already exists"
    username=""
    continue
fi
```
Di dalam loop tersebut, user akan diminta untuk memasukkan username, lalu dicek apakah user tersebut telah terdaftar, apabila iya, akan dimunculkan sebuah pesan error. User akan diminta untuk memasukkan ulang usernamenya setelah itu. Sebuah log juga akan dimasukkan ke dalam file `log.txt` dengan memanggil fungsi `add_log` yang berisi
```sh
add_log() {
    echo "$(date +'%m/%d/%y') $(date +'%T') $1: $2" >> log.txt
}
```
> fungsi `add_log` akan otomatis menambahkan tanggal dan waktu log tersebut dibuat

```sh
echo -n "password : 🔑" 
read -s password

echo -e "\n${YELLOW}------------------"
echo -e '\n'

# Cek apakah password sudah memenuhi kriteria
check_password "$username" "$password"
```
Setelah berhasil memasukkan username, pengguna akan diminta untuk memasukkan password, dan dicek menggunakan fungsi `check_password` yang berisi:
```sh
check_password() {
    clear 
    
    is_eight_character=false
    has_capital=false
    is_alphanumeric=false
    is_not_same=false

    if [ $2 != $1 ]
    then
        is_not_same=true
    else
        echo -e "${RED}Password tidak boleh sama dengan username"
    fi

    if echo $2 | grep -P -q ".{8,}" 
    then
        is_eight_character=true
    else
        echo -e "${RED}Password harus terdiri dari setidaknya 8 karakter"
    fi

    if echo $2 | grep -P -q "(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]+"
    then
        has_capital=true
    else
        echo -e "${RED}Password harus memiliki setidaknya 1 huruf kecil dan 1 huruf besar"
    fi

    if echo $2 | grep -P -q "(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9]+" 
    then
        is_alphanumeric=true
    else
        echo -e "${RED}Password harus terdiri dari angka dan huruf"
    fi

    if [[ $is_eight_character == true && $has_capital == true && $is_alphanumeric == true && $is_not_same == true ]]
    then
        is_password_validated=true
    fi

    echo -e '\n'
}
```
> Terdapat beberapa variable yang bersifat seperti boolean, yang digunakan untuk mengecek apakah password tersebut valid atau tidak
> Pengecekan password dilakukan menggunakan regular expression dan dilakukan satu per satu untuk tiap kondisi

```sh
echo "$username $password" >> ./users/user.txt
add_log "REGISTER" "INFO User $username registered successfully"

echo "Berhasil mendaftarkan $username"
``` 
Setelah berhasil daftar, akan muncul sebuah pesan bahwa user berhasil daftar. 

Pada file `main.sh`, terdapat loop yang digunakan untuk menerima input dan password dari user, dengan sedikit perubahan di pengecekannya.
```
sh
# Mengecek apakah username telah terdaftar
    a=$(awk -v user="$username" -v pass="$password" '$0 ~ user {if ($1 == user && $2 == pass) print "true"; else print "false";}' ./users/user.txt)
    if [[ $a == "true" ]]
    then
        clear
        echo -e "${GREEN}Login Berhasil\n${YELLOW}"
        add_log "LOGIN" "INFO User $username logged in"
        is_password_validated=true
        continue
    else
        clear
        echo -e "${RED}Login GAGAL\n"
        add_log "LOGIN" "ERROR Failed login attemp on user $username"
        username=""
        continue
    fi
```
> username dan password dari `users.txt` diambil menggunakan awk, lalu dicek apakah sama dengan yang diinput oleh user

```sh
while true
do
    read r1 r2
    
    # Memilih menu
    if [[ $r1 == "dl" ]]
        ...
    then
    
    elif [[ $r1 == "att" ]]
    then
        ...
    fi
done
```
Lalu user akan diminta untuk memasukkan salah satu dari dua perintah yang ada

```sh
dirname=$(date +'%F'_$username)
img_num=0
[ -d $dirname ] && rm -r $dirname

if [ -f $dirname.zip ]
then
    unzip -qq -P $password $dirname.zip
    rm $dirname.zip
    latest_img=$(echo $(ls $dirname | tail -1) | tr -d -c 0-9)
    img_num=$(echo $latest_img | sed 's/^0*//')
fi
```
Akan dibuat sebuah folder yang digunakan untuk menampung gambar-gambar yang didownload. Folder tersebut dinamakan berdasarkan waktu dan user yang sedang aktif.

```sh
mkdir -p $dirname

img_num=$((img_num + 1))
for ((num=$img_num; num<=$r2; num=num+1))
do
    filename=$(printf "%02d" $num)
    let prog=$((20 * num / $r2))
    progress=$(for ((i=1; i<=$prog; i=i+1)); do echo -n "#";done)$(for ((i=1; i<=20-$prog; i=i+1)); do echo -n " ";done)
    echo -ne "Downloading '$progress' ($num / $r2) \r"
    curl -L -s https://loremflickr.com/320/240 --output $dirname/PIC_$filename.jpg
done

echo -e "\033[K"
echo -e "${GREEN}Download selesai\n${YELLOW}"

files=($(ls $dirname))
files=(${files[@]/#/$dirname/})

zip -r -qq -P $password "$dirname.zip" "${files[@]:0:$r2}"
```
Gambar yang sudah didownload dan dimasukkan ke dalam folder akan di-compress menjadi sebuah file zip dengan nama yang sama, dan diberi proteksi password menggunakan password user yang sedang aktif.
```sh
clear

echo -e "${CYAN}USER $username telah login sebanyak $(awk -v user=LOGIN.+INFO.+$username '$0 ~ user' log.txt  | wc -l) kali"
echo -e "Ada $(awk -v user=LOGIN.+ERROR.+$username '$0 ~ user {print user}' log.txt | wc -l) kali percobaan login yang gagal${YELLOW}"
```
Perintah att akan menampilkan jumlah percobaan login yang dilakukan. Angka-angka tersebut didapat dari `log.txt` menggunakan awk

## Dokumentasi dan Kendala
- Pengerjaan register.sh
![register.sh](/uploads/7d68f9bc9854966aee3011b5be09023b/image.png)
- Pengerjaan main.sh
![main.sh](/uploads/4a56e36043037e68ee00f9c6a0f6c412/image.png)

Ada beberapa kendala yang timbul saat melakukan pengerjaan soal nomor 1, diantaranya adalah:
1. Terjadi kesulitan saat membuat pengecekan format password. Saya menggunakan operator `=~` untuk regex, tapi ternyata operator tersebut tidak support regex lookahead, dan akhirnya saya menggunakan `grep -P` untuk operasi regexnya
2. Pada perintah `dl N`, apabila sudah ada file zip dengan nama yang sama, zip tersebut harus di-extract dan digabungkan dengan gambar yang baru. Sebelumnya saya hanya melakukan download ulang seluruh gambarnya, lalu diubah menjadi download gambar baru apabila `N` yang ada di zip lebih kecil dari `N` yang baru diinput

# Soal 2
### Penjelasan Soal
Pada soal nomor 2 ini diberikan file log yang berisi IP adress, tanggal akses, jam akses, request-request, dan user yang mengakses pada website daffainfo. Lalu, dengan file ini diperintahkan untuk membuat script awk bernama "soal2_forensic_dapos.sh" untuk membaca file log tersebut.

## 2.a
Membuat folder bernama "forensic_log_website_daffainfo_log" . Nantinya folder ini akan diisi oleh file txt yang berisi hasil dari script awk yang telah dibuat.
```sh
mkdir -p forensic_log_website_daffainfo_log
log_daffainfo=./log_website_daffainfo.log
folder_daffainfo=./forensic_log_website_daffainfo_log
```

>`log_daffainfo` merupakan inisialisasi dari file log
>`folder_daffainfo` merupakan inisialisasi dari folder forensic_log_website_daffainfo_log yang sudah dibuat

```sh
touch $folder_daffainfo/ratarata.txt
touch $folder_daffainfo/result.txt
echo "" > $folder_daffainfo/ratarata.txt
echo "" > $folder_daffainfo/result.txt
```
>`ratarata.txt` untuk menyimpan hasil dari rata rata request
>`result.txt` untuk menyimpan hasil IP yang melakukan request paling banyak, pengguna yang menggunakan user-agent curl, dan daftar IP yang akses pada tanggal 22 jam 2 pagi

## 2.b
Memasukkan jumlah rata-rata request per jam ke dalam sebuah file ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

```sh
time_start=$(awk -F ":" 'NR==2 {print $3}' $log_daffainfo)
time_end=$(awk -F ":" 'END{print $3}' $log_daffainfo)
let hour_elapsed=$time_end-$time_start
cat $log_daffainfo |
awk -v hour=$hour_elapsed ' END {print "Rata-rata serangan adalah sebanyak", (NR-1)/hour, "request per jam"}'>>$folder_daffainfo/ratarata.txt
```
>`time_start` adalah waktu pertama user akses ke dalam website
>`time_end` adalah waktu terakhir user mengakses

Menghitung rata ratanya dengan jumlah serangan `NR-1` dibagi dengan `hour` yang merupakan pengurangan dari `time_end` dan `time_start` lalu, agar bisa terbaca dimasukkan ke dalam file `ratarata.txt` yang berada dalam folder yang sudah dibuat sebelumnya.

## 2.c
Menampilkan IP yang paling banyak melakukan request dan menampilkan jumlah serangan yang dikirimkan.

```sh
cat $log_daffainfo |
awk -F\" '{print $2}' | sort | uniq -c | sort -rn | head -n1 |
awk ' {print "IP yang paling banyak mengakses server adalah", $2, "sebanyak", $1, "request"}'>>$folder_daffainfo/result.txt
```
>menggunakan `sort` dan `uniq -c` untuk mengidentifikasi dan menghitung request
>`sort -rn` untuk megurutkan secara descending
>`head -n1` untuk mengambil IP dan jumlah request dari urutan yang paling atas

## 2.d
Membaca banyaknya requests yang menggunakan user-agent curl
```sh
printf "\n">>$folder_daffainfo/result.txt
cat $log_daffainfo |
awk ' /curl/ {++n}
END { print "Ada", n, "request yang menggunakan curl sebagai user-agent"}'>>$folder_daffainfo/result.txt
```
>menggunakan `/curl/` untuk mencari user-agent curl
`{++n}` untuk melakukan perhitungan request dari user tersebut dan hasilnya akan diletakkan di file result.txt

## 2.e
Mencari daftar IP yang mengakses server pada tanggal 22 jam 2 pagi.

```sh
printf "\n">>$folder_daffainfo/result.txt
cat $log_daffainfo |
awk -F\" ' /2022:02/ {print $2}' | uniq>>$folder_daffainfo/result.txt
```
>`/2022:02/` untuk mencari IP yang mengakses server pada jam 2 pagi

## Dokumentasi Pengerjaan
- Pembuatan script awk
![script_awk](/uploads/858d6240a6cb1ad7acc19025db26b994/script_awk.jpeg)
- hasil file ratarata.txt
![hasil_ratarata](/uploads/bf37f9b4298182a78e9299d7060bbcec/hasil_ratarata.jpeg)
- hasil file result.txt
![result](/uploads/e5dc98cd3ac4a3383ffed749494cf733/result.jpeg)

## Kendala
Kendala-kendala saat progress pengerjaan soal nomor 2 :
1. Pada nomor 2b, sulit untuk mencari cara bagaimana menghitung rata-rata dengan hasil yang benar. Karena pada awalnya pembagi dari rumus rata-rata, saya masukkan 12 karena dilihat dari file log, lamanya user mengakses web adalah sebanyak 12 jam. tetapi ternyata setelah diganti pembaginya menjadi `hour` banyaknya user mengakses website adalah sebanyak 11 jam 48 menit.

# Soal 3
Praktikan diminta untuk membuat `program monitoring recource` pada komputer dengan memanfaatkan command `free -m` dan `du -sh <target_path>`. 
## 3 a.
Membuat script `minutes_log.sh` yang berfungsi untuk mengambil metrics-metrics yang diperlukan dan menyimpan hasil ke dalam file log berformat `metrics_{YmdHms}.log`. Contoh isi file .log:
```sh
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```
## Penyelesaian
Membuat sekaligus membuka script`minute_log.sh`
```sh
nano minute_log.sh
```
> Mengambil nama user serta membuat folder `/home/userName/log`
```sh
user=$(whoami)
mkdir -p "/home/$user/log"
```
Apabila kita mencoba menjalankan`free -m`, maka kia akan mendapatkan hasil berupa tabel berikut:
|        |total| used  | free | shared | buff/cache | available |
| :------|:---:|:-----:|:----:|:------:|:----------:|:---------:|
| Mem:   | 971 | 592   | 136  | 4      | 242        | 236       |
| Swap:  | 448 | 104   | 344  |
Untuk mengambil data, maka diperlukan fungsi `awk`. Sebagai contoh, `code` berikut ini berfungsi untuk mengambil `mem_total`:
```sh
memTotal=$(free -m | grep Mem: |awk '{print $2}')
```
> output dari command `free -m` sebagai input command `grep`, command grep ini akan mencari baris yang mengandung kata `Mem:`. Baris yang dihasilkan dari `grep` akan dijadikan input fungsi `awk`, di mana `awk` akan mengambil nilai pada kolom kedua

Dengan menggunakan konsep yang sama, maka dibuatlah `code` berikut untuk mengambil data lainnya:
```sh
memUsed=$(free -m | grep Mem: |awk '{print $3}')
memFree=$(free -m | grep Mem: |awk '{print $4}')
memShared=$(free -m | grep Mem: |awk '{print $5}')
memBuff=$(free -m | grep Mem: |awk '{print $6}')
memAvail=$(free -m | grep Mem: |awk '{print $7}')
swapTotal=$(free -m | grep Swap: |awk '{print $2}')
swapUsed=$(free -m | grep Swap: |awk '{print $3}')
swapFree=$(free -m | grep Swap: |awk '{print $4}')
```
Adapun command `du -sh /target/path` akan menghasilkan data seperti berikut:
```sh
78M /home/surya
```
Dengan menggunakan cara yang sama pada pengambilan data `free -m`, maka code berikut berfungsi untuk mengambil aspek `path_size`:
```sh
pathSize=$(du -sh /home/$user/ | grep /home/$user/ | awk '{print $1}')
```
Membuat nama variabel  `fileName` yang diperlukan untuk membuat nama file yang sesuai:
```sh
fileName=$(date "+%EY%m%d%H%M%S")
```
Memasukkan hasil script ke dalam file .log. 
```sh
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/metrics_$fileName.log
echo "$memTotal,$memUsed,$memFree,$memShared,$memBuff,$memAvail,$swapTotal,$swapUsed,$swapFree,/home/$user/,$pathSize" >> /home/$user/log/metrics_$fileName.log
```
> Redirection `>` berguna untuk membuat sekaligus memasukkan data ke file .log

Mengubah izin script dengan manjalankan code berikut pada terminal:
```sh
chmod u+x minute_log.sh
```
## 3 b.
Membuat script `minutes_log.sh` berjalan otomatis setiap menit

## Penyelesaian
> untuk melakukan hal ini dapat digunakan `CRON`

Dalam melakukan CRON kita harus melakukan hal berikut untuk menginstall CRON. Jalankan code berikut satu-persatu pada terminal Anda
```sh
sudo apt update
sudo apt install cron
sudo systemctl enable cron
```
Setelah penginstallan berhasil, maka jalankan code ini di terminal
```sh
crontab -e
```
> Anda akan melihat beberapa opsi teks editor yang dapat digunakan, pilihlah teks editor sesuai selera Anda

Ketika script `cron` terbuka, maka tambahkan code berikut pada line terakhir:
```sh
* * * * * /bin/bash /path/menuju/minute_log.sh
```
> Parameter `* * * * *` akan membuat script dijalankan setiap menit

Lalu simpan hasil perubahan dan CRON pun akan berjalan otomatis
## 3 c.
Membuat script `aggregate_minutes_to_hourly_log.sh`. Script ini akan mencari nilai `max`, `min`, dan `avg` file metrics setiap jamnya secara otomatis. Hasil tersebut akan disimpan pada file log berformat `metrics_agg_{YmdH}`. Contoh isi file log:
```sh
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maksimum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1995.5,/home/user/test,62
```
## Penyelesaian
Membuat sekaligus membuka script `aggregate_minutes_to_hourly_log.sh`
```sh
nano aggregate_minutes_to_hourly_log.sh
```
Mengambil nama user dan membuat variabel `fileName` yang berfungsi mempermudah pencarian file-file log permenit berdasar jam yang sama
```sh
user=$(whoami)
fileName=$(date "+%EY%m%d%H")
```
Code berikut berguna untuk mencari nilai `max`
```sh
max_memTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $1}' | sort | tail -1)
```
command `cat` berguna untuk menampilkan isi dari file log permenit berdasar jam yang sama
> `metrics_$fileName*.log` akan mengambil semua file log permenit pada jam yang sama

Dari output command `cat` akan dicari baris yang mengandung kata `/home/`, yakni baris kedua. Dari baris tersebut maka dilakukan `awk`, penambahan `-F ","` berguna untuk merubah separator menjadi `,`, action yang akan dilakukan berupa memprint kolom pertama, yakni mem_total. Data-data mem_total dari semua file akan di-`sort`. Hasil dari command sort ini akan berupa ascending, sehingga untuk mengambil nilai max, maka digunakan `tail -1`. Untuk mengambil nilai `min` digunakan cara yang sama, tetapi merubah `tail` menjadi `head` agar didapatkan nilai terkecil.
```sh
min_memTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $1}' | sort | head -1)
```
Untuk mencari nilai `avg` dapat digunakan `code` berikut
```sh
avg_memTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $1; count++ } END { print total/count }')
```
Perbedaan pencarian `avg` dengan `max` dan `min` ada pada `awk`. awk pada avg melakukan action berupa penambahan nilai variabel total terhadap nilai nilai mem_total dengan nilai total itu sendiri. Selain itu, terdapat variabel `count` sebagai penghitung banyaknya data yang ada. Setelah action selesai dilakukan hingga tidak ditemukan data pada jam yang bersangkutan, awk akan melakukan `print total/count` yakni rumus rata-rata.
> Apabila Anda mencoba menjalankan dan menemukan hasil desimal dengan pemisah koma `,` maka `code` berikut perlu ditambahkan agar dapat sesuai dengan soal yang diminta. Fungsi ini bertujuan merubah karakter `,` menjadi `.`

```sh
avg_memTotal=${avg_memTotal/,/.}
```
Dengan menerapkan cara yang sama, maka bisa didapatkan code untuk mengambil nilai aspek lain sebagai berikut:
```sh
max_memUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $2}' | sort | tail -1)
min_memUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $2}' | sort | head -1)
avg_memUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $2; count++ } END { print total/count }')
avg_memUsed=${avg_memUsed/,/.}

max_memFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $3}' | sort | tail -1)
min_memFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $3}' | sort | head -1)
avg_memFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $3; count++ } END { print total/count }')
avg_memFree=${avg_memFree/,/.}

max_memShared=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $4}' | sort | tail -1)
min_memShared=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $4}' | sort | head -1)
avg_memShared=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $4; count++ } END { print total/count }')
avg_memShared=${avg_memShared/,/.}

max_memBuff=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $5}' | sort | tail -1)
min_memBuff=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $5}' | sort | head -1)
avg_memBuff=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $5; count++ } END { print total/count }')
avg_memBuff=${avg_memBuff/,/.}

max_memAvail=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $6}' | sort | tail -1)
min_memAvail=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $6}' | sort | head -1)
avg_memAvail=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $6; count++ } END { print total/count }')
avg_memAvail=${avg_memAvail/,/.}

max_swapTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $7}' | sort | tail -1)
min_swapTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $7}' | sort | head -1)
avg_swapTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $7; count++ } END { print total/count }')
avg_swapTotal=${avg_swapTotal/,/.}

max_swapUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $8}' | sort | tail -1)
min_swapUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $8}' | sort | head -1)
avg_swapUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $8; count++ } END { print total/count }')
avg_swapUsed=${avg_swapUsed/,/.}

max_swapFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $9}' | sort | tail -1)
min_swapFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $9}' | sort | head -1)
avg_swapFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $9; count++ } END { print total/count }')
avg_swapFree=${avg_swapFree/,/.}

max_pathSize=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $11}' | sort | tail -1)
min_pathSize=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $11}' | sort | head -1)
avg_pathSize=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F"," '{print $11}' | awk '{ total += $1; count++ } END { print total/count }')
avg_pathSize=${avg_pathSize/,/.}
```
> Meski pada pathsize terdapat karakter satuan ukuran, seperti M, G, dll. Namun, keberadaan karakter ini tidak akan mengganggu perhitungan yang ada.

Berdasar soal yang ada satuan `path_size` tidak diperlukan. Namun, Anda dapat menambahkan code berikut apabila ingin menambahkan satuan `path_size`
```sh
#sizeFormat="${max_pathSize:(-1)}"
```
Lakukan command `chmod` untuk mengganti izin file, dengan menjalankan code berikut pada `terminal`
```sh
chmod u+x aggregate_minutes_to_hourly_log.sh
```
Untuk dapat menjalankan script secara otomatis, maka kita perlu menambahkan `CRON` dengan cara
1. Menjalankan `crontab -e` pada terminal
2. Menambahkab `59 * * * * /bin/bash /path/menuju/aggregate_minutes_to_hourly_log.sh` pada file cron. Parameter ini akan membuat script dijalankan setiap menit 59
3. Simpan perubahan yang dibuat

## 3 d.
Merubah izin file log menjadi hanya bisa dibaca oleh user pemilik file saja
## Penyelesaian
Untuk melakukan hal tersebut kita dapat menggunakan command `chmod` pada script `minute_log.sh` dan `aggregate_minutes_to_hourly_log.sh`
```sh
chmod 400 /home/$user/log/metrics_$fileName.log
```
Code di atas dapat ditambahkan pada line terakhir script `minute_log.sh`
```sh
chmod 400 /home/$user/log/metrics_agg_$fileName.log
```
Code di atas dapat ditambahkan pada line terakhir script`aggregate_minutes_to_hourly_log.sh`
> Pemilihan nilai 4 0 0 sesuai dengan panduan `chmod` dimana 4 (`read only`) untuk `user`, 0 pertama untuk `group` (`none`), dan 0 kedua untuk `Others` (`none`)

## Dokumentasi Pengerjaan
Berikut beberapa dokumentasi pengerjaan
- Analisis dan Pemahaman Soal
![WhatsApp_Image_2022-03-05_at_20.07.51](/uploads/b06768d2dedbf39ac9a3514b3a8a4ceb/WhatsApp_Image_2022-03-05_at_20.07.51.jpeg)
- Pembuatan script `minutes_log.sh`
![minute_log.sh](/uploads/37c78bf653808ab3159b0dccc540edb2/minute_log.sh.jpeg)
- Pembuatan `CRON`
![cron](/uploads/05fcd0f704212c2a910389a19a6d27fb/cron.jpeg)
- Pembuatan script `aggregate_minutes_to_hourly_log.sh`
![aggregate.sh](/uploads/3368d1f63066435e77ac7310ba74f3f3/aggregate.sh.jpeg)
- Penambahan `chmod 400`
![chmod_400](/uploads/8c355ac1550e87c5a0fe6d9c946d39aa/chmod_400.jpeg)
## Kendala dalam Pengerjaan
1. Kendala yang ada ada pada script `aggregate_minutes_to_hourly_log.sh`, yakni menemukan bagaimana cara untuk mengambil data-data dari banyak file. Hal ini ternyata dapat dilakukan dengan menggunakan `.../metrics_$fileName*.log` dimana `*` membuat data pada menit atau detik apapun akan terbaca.
2. Selain itu, perubahan sistem operasi yang digunakan cukup mempengaruhi perfoma pengerjaan.
3. Pembuatan CRON yang tidak dapat berjalan. Solusi yang didapat dari hal ini adalah menambah path secara lengkap, melakukan `chmod u+x` pada script, dan penambahan `/bin/bash`

