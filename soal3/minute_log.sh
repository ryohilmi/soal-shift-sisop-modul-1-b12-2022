#!/bin/bash
user=$(whoami)
mkdir -p "/home/$user/log"

memTotal=$(free -m | grep Mem: |awk '{print $2}')
memUsed=$(free -m | grep Mem: |awk '{print $3}')
memFree=$(free -m | grep Mem: |awk '{print $4}')
memShared=$(free -m | grep Mem: |awk '{print $5}')
memBuff=$(free -m | grep Mem: |awk '{print $6}')
memAvail=$(free -m | grep Mem: |awk '{print $7}')
swapTotal=$(free -m | grep Swap: |awk '{print $2}')
swapUsed=$(free -m | grep Swap: |awk '{print $3}')
swapFree=$(free -m | grep Swap: |awk '{print $4}')
pathSize=$(du -sh /home/$user/ | grep /home/$user/ | awk '{print $1}')

fileName=$(date "+%EY%m%d%H%M%S")

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/metrics_$fileName.log
echo "$memTotal,$memUsed,$memFree,$memShared,$memBuff,$memAvail,$swapTotal,$swapUsed,$swapFree,/home/$user/,$pathSize" >> /home/$user/log/metrics_$fileName.log

chmod u+x ./minute_log.sh
chmod 400 /home/$user/log/metrics_$fileName.log
