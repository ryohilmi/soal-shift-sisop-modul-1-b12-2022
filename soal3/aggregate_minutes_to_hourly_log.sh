#!/bin/bash

user=$(whoami)
fileName=$(date "+%EY%m%d%H")
#fileName="2022022619"

max_memTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $1}' | sort | tail -1)
min_memTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $1}' | sort | head -1)
avg_memTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $1; count++ } END { print total/count }')
avg_memTotal=${avg_memTotal/,/.}

max_memUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $2}' | sort | tail -1)
min_memUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $2}' | sort | head -1)
avg_memUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $2; count++ } END { print total/count }')
avg_memUsed=${avg_memUsed/,/.}

max_memFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $3}' | sort | tail -1)
min_memFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $3}' | sort | head -1)
avg_memFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $3; count++ } END { print total/count }')
avg_memFree=${avg_memFree/,/.}

max_memShared=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $4}' | sort | tail -1)
min_memShared=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $4}' | sort | head -1)
avg_memShared=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $4; count++ } END { print total/count }')
avg_memShared=${avg_memShared/,/.}

max_memBuff=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $5}' | sort | tail -1)
min_memBuff=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $5}' | sort | head -1)
avg_memBuff=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $5; count++ } END { print total/count }')
avg_memBuff=${avg_memBuff/,/.}

max_memAvail=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $6}' | sort | tail -1)
min_memAvail=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $6}' | sort | head -1)
avg_memAvail=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $6; count++ } END { print total/count }')
avg_memAvail=${avg_memAvail/,/.}

max_swapTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $7}' | sort | tail -1)
min_swapTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $7}' | sort | head -1)
avg_swapTotal=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $7; count++ } END { print total/count }')
avg_swapTotal=${avg_swapTotal/,/.}

max_swapUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $8}' | sort | tail -1)
min_swapUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $8}' | sort | head -1)
avg_swapUsed=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $8; count++ } END { print total/count }')
avg_swapUsed=${avg_swapUsed/,/.}

max_swapFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $9}' | sort | tail -1)
min_swapFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $9}' | sort | head -1)
avg_swapFree=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{ total += $9; count++ } END { print total/count }')
avg_swapFree=${avg_swapFree/,/.}

max_pathSize=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $11}' | sort | tail -1)
min_pathSize=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F "," '{print $11}' | sort | head -1)
avg_pathSize=$(cat /home/$user/log/metrics_$fileName*.log | grep /home/ | awk -F"," '{print $11}' | awk '{ total += $1; count++ } END { print total/count }')
avg_pathSize=${avg_pathSize/,/.}
#sizeFormat="${max_pathSize:(-1)}"

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/metrics_agg_$fileName.log
echo "minimum,$min_memTotal,$min_memUsed,$min_memFree,$min_memShared,$min_memBuff,$min_memAvail,$min_swapTotal,$min_swapUsed,$min_swapFree,/home/$user/,$min_pathSize" >> /home/$user/log/metrics_agg_$fileName.log
echo "maximum,$max_memTotal,$max_memUsed,$max_memFree,$max_memShared,$max_memBuff,$max_memAvail,$max_swapTotal,$max_swapUsed,$max_swapFree,/home/$user/,$max_pathSize" >> /home/$user/log/metrics_agg_$fileName.log
echo "average,$avg_memTotal,$avg_memUsed,$avg_memFree,$avg_memShared,$avg_memBuff,$avg_memAvail,$avg_swapTotal,$avg_swapUsed,$avg_swapFree,/home/$user/,$avg_pathSize" >> /home/$user/log/metrics_agg_$fileName.log

chmod u+x ./aggregate_minutes_to_hourly_log.sh
chmod 400 /home/$user/log/metrics_agg_$fileName.log
