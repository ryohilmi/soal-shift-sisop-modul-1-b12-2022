#!/bin/bash

# (2A) membuat folder
mkdir -p forensic_log_website_daffainfo_log
log_daffainfo=./log_website_daffainfo.log
folder_daffainfo=./forensic_log_website_daffainfo_log

touch $folder_daffainfo/ratarata.txt
touch $folder_daffainfo/result.txt
echo "" > $folder_daffainfo/ratarata.txt
echo "" > $folder_daffainfo/result.txt

# (2B) rata-rata request per jam
time_start=$(awk -F ":" 'NR==2 {print $3}' $log_daffainfo)
time_end=$(awk -F ":" 'END{print $3}' $log_daffainfo)
let hour_elapsed=$time_end-$time_start
cat $log_daffainfo |
awk -v hour=$hour_elapsed ' END {print "Rata-rata serangan adalah sebanyak", (NR-1)/hour, "request per jam"}'>>$folder_daffainfo/ratarata.txt

# (2C) IP paling banyak request
cat $log_daffainfo |
awk -F\" '{print $2}' | sort | uniq -c | sort -rn | head -n1 |
awk ' {print "IP yang paling banyak mengakses server adalah", $2, "sebanyak", $1, "request"}'>>$folder_daffainfo/result.txt

# (2D) banyak request menggunakan user-agent curl
printf "\n">>$folder_daffainfo/result.txt
cat $log_daffainfo |
awk ' /curl/ {++n}
END { print "Ada", n, "request yang menggunakan curl sebagai user-agent"}'>>$folder_daffainfo/result.txt
 
# (2E) daftar IP yang mengakses pada jam 2 pagi tgl 22
printf "\n">>$folder_daffainfo/result.txt
cat $log_daffainfo |
awk -F\" ' /2022:02/ {print $2}' | uniq>>$folder_daffainfo/result.txt
